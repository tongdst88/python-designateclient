%global _empty_manifest_terminate_build 0
Name:           python-designateclient
Version:        4.5.0
Release:        1
Summary:        OpenStack DNS-as-a-Service - Client
License:        Apache-2.0
URL:            https://docs.openstack.org/python-designateclient/latest
Source0:        https://files.pythonhosted.org/packages/bc/06/4089e5e541962284e0c4ce18c8933fd986ec7777b8999fbc5c07d7854868/python-designateclient-4.5.0.tar.gz
BuildArch:      noarch
%description
Client library and command line utility for interacting with Openstack Designate API

%package -n python3-designateclient
Summary:        OpenStack DNS-as-a-Service - Client
Provides:       python-designateclient
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-cliff
BuildRequires:  python3-debtcollector
BuildRequires:  python3-jsonschema
BuildRequires:  python3-keystoneauth1
BuildRequires:  python3-osc-lib
BuildRequires:  python3-oslo-serialization
BuildRequires:  python3-oslo-utils
BuildRequires:  python3-requests
BuildRequires:  python3-stevedore
# General requires
Requires:       python3-cliff
Requires:       python3-debtcollector
Requires:       python3-jsonschema
Requires:       python3-keystoneauth1
Requires:       python3-osc-lib
Requires:       python3-oslo-serialization
Requires:       python3-oslo-utils
Requires:       python3-pbr
Requires:       python3-requests
Requires:       python3-stevedore
%description -n python3-designateclient
Client library and command line utility for interacting with Openstack Designate API

%package help
Summary:        OpenStack DNS-as-a-Service - Client
Provides:       python3-designateclient-doc
%description help
Client library and command line utility for interacting with Openstack Designate API

%prep
%autosetup -n python-designateclient-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-designateclient -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Jul 12 2022 renliang16 <renliang@uniontech.com> - 4.5.0-1
- Upgrade package python3-designateclient to version 4.5.0

* Fri Jul 09 2021 openstack-sig <openstack@openeuler.org>
- update to 4.2.0

* Thu Jan 07 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
